﻿CREATE TABLE [dbo].[TEAM] (
    [name]               VARCHAR (50) NOT NULL,
    [logo]               VARCHAR (50) NULL,
    [games]              INT          NULL,
    [wins]               INT          NULL,
    [losses]             INT          NULL,
    [points]             INT          NULL,
    [pointsFor]          INT          NULL,
    [pointsAgainst]      INT          NULL,
    [pointsDifferential] INT          NULL,
    [logo45x41]			 VARCHAR(50)  NULL, 
    PRIMARY KEY CLUSTERED ([name] ASC)
);

CREATE TABLE [dbo].[GAME] (
    [Id]             INT          NOT NULL,
    [date]           DATE         NULL,
    [homeTeam]       VARCHAR (50) NOT NULL,
    [awayTeam]       VARCHAR (50) NOT NULL,
    [homeTeam_score] INT          NULL,
    [awayTeam_score] INT          NULL,
    [winner]         VARCHAR (50) NULL,
    [loser]          VARCHAR (50) NULL,
    [round]          INT          NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

SQL Management Studio:
CREATE TABLE Team (
    teamName			VARCHAR (100) PRIMARY KEY	NOT NULL,
    logo				VARCHAR (100)				NULL,
    games				INT							NULL,
    wins				INT							NULL,
    losses				INT							NULL,
    points				INT							NULL,
    pointsFor			INT							NULL,
    pointsAgainst		INT							NULL,
    pointsDifferential	INT							NULL,
    logo45x41			VARCHAR(100)				NULL
);

CREATE TABLE Game (
    Id					INT	IDENTITY(1,1)			NOT NULL,
    homeTeam			VARCHAR (100)				NOT NULL,
    awayTeam			VARCHAR (100)				NOT NULL,
	roundNr				INT							NOT NULL,
    homeTeam_score		INT							NULL,
    awayTeam_score		INT							NULL,
    winner				VARCHAR (100)				NULL,
    loser				VARCHAR (100)				NULL,
	dateOfPlay			DATE						NULL
);