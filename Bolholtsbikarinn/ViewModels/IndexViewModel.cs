﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bolholtsbikarinn.Models;

namespace Bolholtsbikarinn.ViewModels
{
    public class IndexViewModel
    {
        public List<Team> AllTeams { get; set; }
        public List<Game> AllGames { get; set; }
        //public List<string> TeamStandings { get; set; }
    }
}