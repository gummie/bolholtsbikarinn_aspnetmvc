﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Bolholtsbikarinn.DAL;
using Bolholtsbikarinn.Models;
using Bolholtsbikarinn.ViewModels;

namespace Bolholtsbikarinn.Controllers
{
    public class HomeController : Controller
    {
        //private Bolholtsbikarinn_dataEntities db = new Bolholtsbikarinn_dataEntities();
        DataRepository dr = new DataRepository();

        public ActionResult Index()
        { 
            IndexViewModel vm = new IndexViewModel();

            vm.AllTeams = dr.GetTeams();
            vm.AllGames = dr.GetGames();
            //vm.TeamStandings = dr.GetTeamStandings();

            return View(vm);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Hér verður hægt að skrá sig inn til að setja inn úrslit o.fl.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}