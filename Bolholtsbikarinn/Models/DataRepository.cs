﻿using System.Collections.Generic;

namespace Bolholtsbikarinn.Models
{
    public class DataRepository
    {
        BolholtsbikarinnDatabaseEntities1 db = new BolholtsbikarinnDatabaseEntities1();

        public List<Team> GetTeams()
        {
            List<Team> AllTeams = new List<Team>();

            foreach (var item in db.Team)
            {
                AllTeams.Add(item);
            }

            return AllTeams;
        }

        public List<Game> GetGames()
        {
            List<Game> AllGames = new List<Game>();

            foreach (var item in db.Game)
            {
                AllGames.Add(item);
            }

            return AllGames;
        }

        //public List<TEAM> GetTeamStandings()
        //{
        //    List<TEAM> TeamStandings = new List<TEAM>();

        //    foreach (var team in db.TEAM)
        //    {
        //        TeamStandings.Add(team.name);

        //        var gameCount = 0;
        //        foreach (var games in GetGames())
        //        {
        //            if (games.homeTeam.Equals(team.name) || games.awayTeam.Equals(team.name))
        //            {
        //                if (games.homeTeam_score != null)
        //                {
        //                    gameCount++;
        //                }
        //            }
        //        }
        //        TeamStandings.Add(gameCount.ToString());

        //        var winCount = 0;
        //        foreach (var games in GetGames())
        //        {
        //            if (games.winner != null)
        //            {
        //                if (games.winner.Equals(team.name))
        //                {
        //                    winCount++;
        //                }
        //            }
        //        }
        //        TeamStandings.Add(winCount.ToString());

        //        var lossCount = 0;
        //        foreach (var games in GetGames())
        //        {
        //            if (games.loser != null)
        //            {
        //                if (games.loser.Equals(team.name))
        //                {
        //                    lossCount++;
        //                }
        //            }
        //        }
        //        TeamStandings.Add(lossCount.ToString());

        //        var points = 0;
        //        points = winCount * 2;
        //        TeamStandings.Add(points.ToString());

        //        var pointsFor = 0;
        //        var pointsAgainst = 0;
        //        var totalPoints = 0;
        //        string allPoints = string.Empty;
        //        foreach (var score in GetGames())
        //        {
        //            if (score.homeTeam_score != null)
        //            {
        //                if (score.homeTeam.Equals(team.name))
        //                {
        //                    pointsFor += score.homeTeam_score.Value;
        //                }
        //                else if (score.awayTeam.Equals(team.name))
        //                {
        //                    pointsFor += score.awayTeam_score.Value;
        //                }
        //            }
        //        }
        //        foreach (var score in GetGames())
        //        {
        //            if (score.homeTeam_score != null)
        //            {
        //                if (score.homeTeam.Equals(team.name))
        //                {
        //                    pointsAgainst += score.awayTeam_score.Value;
        //                }
        //                else if (score.awayTeam.Equals(team.name))
        //                {
        //                    pointsAgainst += score.homeTeam_score.Value;
        //                }
        //            }
        //        }

        //        totalPoints = pointsFor - pointsAgainst;
        //        allPoints = pointsFor.ToString() + "  -  " +
        //                    pointsAgainst.ToString() + "   =    " +
        //                    totalPoints.ToString();

        //        TeamStandings.Add(allPoints);
        //    }
        //    return TeamStandings;
        //}
    }

            // TEST DATA below----------------

        //    //return new List<Team> {
        //    //                new Team () {  Name = "Sérdeildin", Logo = "Prufa"},
        //    //                new Team () {  Name = "Fjarðabyggð", Logo = "Prufa2"},
        //    //};
        //}

        //public List<Game> GetGames()
        //{
        //    return new List<Game>
        //    {
        //        new Game() {HomeTeamName = "Sérdeildin", AwayTeamName = "Fjarðabyggð", Round = 1}
        //    };
        //}
}
